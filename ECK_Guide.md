

This guide following Elastic Cloud on Kubernetes quickstart guide with the addition of deploying Filebeat `DaemonSet`
with the required adjustment which some of them aren't covered in Elastic documentation hence, this guide.

## Assumptions

* You have a Kubernetes (1.12+) cluster running.
* You have a `storageClass` configured.

## Overview

From Elastic page:
"Built on the Kubernetes Operator pattern, Elastic Cloud on Kubernetes (ECK) extends the basic Kubernetes orchestration capabilities to support the setup and management of Elasticsearch, Kibana and APM Server on Kubernetes."

We will start by following the ECK quickstart guide and after we will deploy it we will extract some data like certificates
and passwords for Elasticsearch, then we will customize Filebeat and will deploy it to the Kubernetes cluster. 


# Deploying ECK

## Deploy The Elastic Operator

Install custom resource definitions and the operator with its RBAC rules, this is relativly long file (2281 lines so it will not be presented here. also, no customization is required):

```CRD
kubectl apply -f https://download.elastic.co/downloads/eck/1.1.2/all-in-one.yaml
```

Monitor the `operator` logs:

```monitorCRD
kubectl -n elastic-system logs -f statefulset.apps/elastic-operator
```

And view the state of the `pod`:

```getCRD
kubectl get pods -n elastic-system
```

## Deploy the Elasticsearch cluster

Create a file with the following content:

```elasticCluster
apiVersion: elasticsearch.k8s.elastic.co/v1
kind: Elasticsearch
metadata:
  name: elasticsearch
  namespace: default
spec:
  version: 7.8.0
  nodeSets:
    - name: default
      config:
        node.master: true
        node.data: true
        node.ingest: true
        node.ml: true
        node.store.allow_mmap: false
        xpack.security.authc.realms:    # Workaround: https://discuss.elastic.co/t/new-user-cant-login-kibana/204810
          native:
            native1: 
              order: 1
      podTemplate:
        metadata:
          labels:
            # additional labels for pods 
            foo: bar
        spec:
          containers:
            - name: elasticsearch
              resources:
                limits:
                  memory: 4Gi
                  cpu: 1
              env:
                - name: ES_JAVA_OPTS
                  value: "-Xms2g -Xmx2g"
      count:
        3
      volumeClaimTemplates:
        - metadata:
            name: elasticsearch-data
          spec:
            accessModes:
              - ReadWriteOnce
            resources:
              requests:
                storage: 5Gi
            storageClassName: standard
```

Make sure to edit:
 * `metadata.name` for the `elasticsearch` name.
 * `metadata.name` for the `namespace` in which the cluster will live.
 * `count:` to the desired size of the cluster.
 * `storageClassName:` to match your `storageClass` name.
 

Apply the above file:

```applyCluster
kubectl apply -f elasticCluster.yaml
```

Monitor the cluster, When you create the cluster, there is no HEALTH status and the PHASE is empty. After a while, the PHASE turns into Ready, and HEALTH becomes green:

```monitorElastic
kubectl kubectl logs -f elasticsearch-0
```

When `elasticsearch` deploy, it will create a secret named 'elastic-es-elastic-user' (`metadata.name`-es-lastic-user) which can be
used to validate elastic status.

Get the password:

```password
PASSWORD=$(kubectl get secret elastic-es-elastic-user -o go-template='{{.data.elastic | base64decode}}')
```

From your local workstation, use the following command in a separate terminal:

```portForward
kubectl port-forward service/elastic-es-http 9200
```

Then connect to elastic:

```portForward
curl -u "elastic:$PASSWORD" -k "https://localhost:9200"
```

```connection
{
  "name" : "quickstart-es-default-0",
  "cluster_name" : "quickstart",
  "cluster_uuid" : "XqWg0xIiRmmEBg4NMhnYPg",
  "version" : {...},
  "tagline" : "You Know, for Search"
}
```

## Deploying Kibana

Create a file with the following content:

```kibana
apiVersion: kibana.k8s.elastic.co/v1
kind: Kibana
metadata:
  name: quickstart
spec:
  version: 7.8.0
  count: 1
  elasticsearchRef:
    name: quickstart
```

Make sure to edit:
 * `elasticsearchRef.name` for the `elasticsearch` name.

Apply the above file:

```applyCluster
kubectl apply -f kibana.yaml
```

Verify the status of Kibana:

```kibanaStatus
kubectl get kibana
```

OK, untill here it's pretty much 1:1 the ECK guide. but we now have a cluster without 
anything that feeds logs.

## Deploying Filebeat

Here we also follow the Filebeat Guide from Elastic but will add TLS certificates that their guide
doesn't covers.(filebeat will also fail without a certificate on x509)

If you deployed elastic with your own certificates(https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-tls-certificates.html#k8s-setting-up-your-own-certificate) use them. If you don't have a certificate and still want to run Filebeat
use the self-signed certificates that Elastic created earlier.

```elasticCerts
kubectl get secret | grep es-http
hulk-es-http-ca-internal         Opaque                                2      10m
hulk-es-http-certs-internal      Opaque                                2      10m
hulk-es-http-certs-public        Opaque                                2      10m
```

We will create new secretes for Filebeat. first, decode the date from the above secrets into new seperated files
```getCerts
kubectl get secret elasti-es-http-ca-internal -o go-template='{{index .data "tls.crt" | base64decode }}' > ca.crt
kubectl get secret elastic-es-http-certs-public -o go-template='{{index .data "tls.crt" | base64decode }}' > tls.crt
kubectl get secret elastic-es-http-certs-internal -o  go-template='{{index .data "tls.key" | base64decode }}' > tls.key
```

Second, Create two new secrets:

```createCerts
kubectl create secret generic filebeat-ca --from-file=ca.crt=ca.crt
kubectl create secret generic filebeat-cert-plus-key --from-file=tls.crt=tls.crt --from-file=tls.key=tls.key
```

Create a file with the following content:

```filebeatYaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: filebeat-config
  namespace: default
  labels:
    k8s-app: filebeat
data:
  filebeat.yml: |-
    filebeat.autodiscover:
      providers:
        - type: kubernetes
          node: ${NODE_NAME}
          hints.enabled: true
          hints.default_config:
            type: container
            paths:
              - /var/log/containers/*${data.kubernetes.container.id}.log

    processors:
      - add_cloud_metadata:
      - add_host_metadata:

    cloud.id: ${ELASTIC_CLOUD_ID}
    cloud.auth: ${ELASTIC_CLOUD_AUTH}

    output.elasticsearch:
      hosts: ['${ELASTICSEARCH_HOST:elasticsearch}:${ELASTICSEARCH_PORT:9200}']
      username: ${ELASTICSEARCH_USERNAME}
      password: ${ELASTICSEARCH_PASSWORD}
      ssl.certificate_authorities: ["/etc/pki/root/ca.crt"]
      ssl.certificate: "/etc/pki/client/tls.crt"
      ssl.key: "/etc/pki/client/tls.key"
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: filebeat
  namespace: logging
  labels:
    k8s-app: filebeat
spec:
  selector:
    matchLabels:
      k8s-app: filebeat
  template:
    metadata:
      labels:
        k8s-app: filebeat
    spec:
      serviceAccountName: filebeat
      terminationGracePeriodSeconds: 30
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      containers:
      - name: filebeat
        image: docker.elastic.co/beats/filebeat:7.7.1
        args: [
          "-c", "/etc/filebeat.yml",
          "-e",
        ]
        env:
        - name: ELASTICSEARCH_HOST
          value: https://elasticsearch-es-http.default.svc
        - name: ELASTICSEARCH_PORT
          value: "9200"
        - name: ELASTICSEARCH_USERNAME
          value: elastic
        - name: ELASTICSEARCH_PASSWORD
          value: "elastic-password"
        - name: NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        securityContext:
          runAsUser: 0
        resources:
          limits:
            memory: 200Mi
          requests:
            cpu: 100m
            memory: 100Mi
        volumeMounts:
        - name: config
          mountPath: /etc/filebeat.yml
          readOnly: true
          subPath: filebeat.yml
        - name: data
          mountPath: /usr/share/filebeat/data
        - name: varlogcontainers
          mountPath: /var/lib/docker/containers
          readOnly: true
        - name: varlog
          mountPath: /var/log
          readOnly: true
        # tls mountPaths
        - mountPath: /etc/pki/root/
          name: filebeat-ca
        - mountPath: /etc/pki/client/
          name: filebeat-cert-plus-key
      volumes:
      - name: config
        configMap:
          defaultMode: 0600
          name: filebeat-config
      - name: varlogcontainers
        hostPath:
          path: /var/log/containers/
      - name: varlog
        hostPath:
          path: /var/log
      - name: data
        hostPath:
          path: /var/lib/filebeat-data
          type: DirectoryOrCreate
      # tls secrets
      - name: filebeat-ca
        secret:
          secretName: filebeat-ca
      - name: filebeat-cert-plus-key
        secret:
          secretName: filebeat-cert-plus-key                 
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: filebeat
subjects:
- kind: ServiceAccount
  name: filebeat
  namespace: logging
roleRef:
  kind: ClusterRole
  name: filebeat
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: filebeat
  labels:
    k8s-app: filebeat
rules:
- apiGroups: [""] # "" indicates the core API group
  resources:
  - namespaces
  - pods
  verbs:
  - get
  - watch
  - list
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: filebeat
  namespace: logging
  labels:
    k8s-app: filebeat
---
```

Make sure to edit:
 * `metadata.namespace` with the desired namespace (preferably the same as Elastic and Kibana)
 * `spec.template.spec.containers.env.name.ELASTICSEARCH_HOST` with elasstic internal fqdn name (https://elasticsearch-es-http.default.svc).
 * `spec.template.spec.containers.env.name.ELASTICSEARCH_PASSWORD` with elastic password.
 * `spec.template.spec.containers.volumeMounts` with the names of the secrets if you changed those.
 * `spec.template.spec.containers.volumes` with the names of the secrets if you changed those.

 Apply the above file:

 ```applyFilebeat
 kubectl apply -f filebeat.yaml
 ```

 Monitor the pods

 ```monitorFluentbeat
 kubectl get pods -n logging |grep fluentbeat
 ```

Monitor the logs and verify that fluentbeat can connect to Elastic:

```fluentbeatLogs
kubectl logs filebeat-xxxxx
```

If no errors are shown in the logs you can now connect to kibana, view your logs and start working. 

following are some Filebeat errors and how to debug them:

```
kubectl logs filebeat-xxxxx
...
ERROR	[publisher_pipeline_output]	pipeline/output.go:155	Failed to connect to backoff(elasticsearch(https://elasticsearch-es-http.default.svc.cluster.local:9200)): Get https://elasticsearch-es-http.default.svc.cluster.local:9200: x509: certificate is valid for elasticsearch-es-http.default.es.local, elasticsearch-es-http, elasticsearch-es-http.default.svc, elasticsearch-es-http.default, not elasticsearch-es-http.default.svc.cluster.local
...
```

In case of a `x509` error, open a shell to filebeat `pod` and verify the connection and certificate:

Connect to the container:

```execFilebeat
kubectl exec -it filebeat-xxxxx -- /bin/bash
```

Use `curl` to verify the connection to elastic, first with `-k` to ignore the certificate:

```curl
bash-4.2$ curl  -k -u "elastic:9Bl42v729KQtpqTE61m21TOt" https://elasticsearch-credit-agricole-es-http.default.svc:9200
{
  "name" : "elasticsearch-es-default-0",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "Wain8y5fTGGlh2AjRuun0g",
  "version" : {
    "number" : "7.8.0",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "757314695644ea9a1dc2fecd26d1a43856725e65",
    "build_date" : "2020-06-14T19:35:50.234439Z",
    "build_snapshot" : false,
    "lucene_version" : "8.5.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}

```

And on the second without:

```curl
bash-4.2$ curl -u "elastic:9Bl42v729KQ9Bl42v729KQtpqTE61m21TOttpqTE61m21TOt" https://elasticsearch-credit-agricole-es-http.default.svc:9200
curl: (60) Peer's certificate issuer has been marked as not trusted by the user.
More details here: http://curl.haxx.se/docs/sslcerts.html
curl performs SSL certificate verification by default, using a "bundle"
 of Certificate Authority (CA) public keys (CA certs). If the default
 bundle file isn't adequate, you can specify an alternate file
 using the --cacert option.
If this HTTPS server uses a certificate signed by a CA represented in
 the bundle, the certificate verification probably failed due to a
 problem with the certificate (it might be expired, or the name might
 not match the domain name in the URL).
If you'd like to turn off curl's verification of the certificate, use
 the -k (or --insecure) option.
```

If you got the above `SSL` error, verify the certificates that you used. you can verify them buy creating
a new file in the `pod` and and using `curl` with `--ca-cert` as follows:

```curlCaCert
curl --cacert ca.crt -u "elastic:9Bl42v729KQtpqTE61m21TOt" https://elasticsearch-credit-agricole-es-http.default.svc:9200
```

If the command succeed, there was a problem mounting the secret to filebeat. If the command fails, check your certificates and key.

If you see the following error in the logs:

```containerPathError
2020-07-01T09:49:30.769Z	ERROR	[kubernetes]	add_kubernetes_metadata/matchers.go:91	Error extracting container id - source value 
does not contain matcher's logs_path '/var/lib/docker/containers/'.
```

It implies that the path chosen for collecting the containers logs is wrong. The path is different between systems running Docker or 
containerD. In Filebeat yaml that we created before edit the `configMap` section(top): 
* contained: `/var/log/containers/*${data.kubernetes.container.id}.log`
* docker: `/var/lib/docker/containers/*${data.kubernetes.container.id}.log`


